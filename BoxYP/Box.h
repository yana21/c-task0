#pragma once
#include<iostream>
namespace Space {

	class Box {
	private:
		int length;
		int width;
		int height;
		double weight;
		int value;
	public:
		Box(int theLength, int theWidth, int theHeight, double theWeight, int theValue);
		Box();

		//2
		void setLength(int length);
		void setWidth(int width);
		void setHeight(int height);
		void setWeight(double weight);
		void setValue(int value);

		int getLength();
		int getWidth();
		int getHeight();
		double getWeight();
		int  getValue();

		friend std::istream& operator>> (std::istream& is, Box& b);
		friend std::ostream& operator<<(std::ostream& s, Box &b);
	};

	
	//перегрузки операторов 
	
	bool operator == (Box &box1, Box& box2);
	std::ostream& operator<<(std::ostream& s, Box &b);
	std::istream& operator>> (std::istream& is, Box& b);

	int  valueCounter(Box* a, int count);
	//3
	bool  checkSum(Box a[], int count, int max);
	//4
	double maxWeight(Box a[], int count, int maxV);
	//5
	bool getInside(Box arr[], int count);
	//6 
	bool isBoxesEquals(Box a, Box b, int count);

}